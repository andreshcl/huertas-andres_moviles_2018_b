package com.example.jimmyhuertas.examen

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_game_main.*

class GameMainActivity : AppCompatActivity() {

    internal lateinit var scoreTextView: TextView
    internal lateinit var targetTextView: TextView
    internal lateinit var tapButton: Button
    internal lateinit var startButton: Button
    internal var score = 0
    internal var target = 0

    internal lateinit var countDownTimer: CountDownTimer
    internal val countDownInterval = 1000L
    internal val initialCountDown = 10000L
    internal var timeLeft = 10


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_main)


        scoreTextView = findViewById(R.id.score_text_view)
        targetTextView = findViewById(R.id.target_text_view)
        tapButton = findViewById(R.id.tap_button)
        startButton = findViewById(R.id.start_button)

        startButton.setOnClickListener { _ -> startGame() }
        tapButton.setOnClickListener { _ -> detener() }

    }

    private fun startGame() {
        score = 0
        val n = (0..10).shuffled().first()
        val targetText = "Target : " + Integer.toString(n)
        target = n
        targetTextView.text = targetText

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000

            }

            override fun onFinish() {
                //endGame()
            }
        }

        countDownTimer.start()


    }

    private fun detener() {

        countDownTimer.cancel()
        var diferencia = timeLeft - target
        var scoreRound = "Your Score : " + Integer.toString(score)


        if (timeLeft === target) {
            score = 100
            scoreTextView.text = scoreRound

        } else if (diferencia === 1 || diferencia === (-1)) {
            score = 50
            scoreTextView.text = scoreRound
        } else {
            score = 0
            scoreTextView.text = scoreRound
        }


    }
}
